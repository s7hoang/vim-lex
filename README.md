This is an indent file for lex files in vim.

There technically already is one that is included with the base installation of
vim but, I found that it doesn't tend to indent everything the way I wanted it.
So, I wrote my own.
