" Vim indent file for Lex
" Language: Lex
" Maintainer: Sunny Hoang

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
  finish
end
let b:did_indent = 1

" Only define the functions once.
if exists('VimLex_Indent')
  finish
endif

if exists('VimLex_DetermineSection')
  finish
endif

if exists('VimLex_InEnclosedBlock')
  finish
endif

if exists('VimLex_InRulesSubsection')
  finish
endif

if exists('VimLex_InEnclosedBlock')
  finish
endif


setlocal indentexpr=VimLex_Indent()

let s:true                 = 1
let s:false                = 0
let s:sol                  = 1
let s:sof                  = 1
let s:notFound             = 0
let s:sectionIndicator     = '^\s*%%\s*$'
let s:openRulesSubsection  = '%{'
let s:closeRulesSubsection = '%}'
let s:firstSection         = 0
let s:secondSection        = 1
let s:thirdSection         = 2

function! VimLex_Indent()
  let l:line    = getline(v:lnum)
  let l:previousNum  = prevnonblank(v:lnum - 1)
  let l:previousLine = getline(previousNum)
  let l:section = VimLex_DetermineSection(v:lnum)

  if line =~ s:sectionIndicator
    return 0
  endif 

  if l:section == s:firstSection && VimLex_InRulesSubsection(v:lnum)
    echom v:lnum . ":" . l:line
    "fix weird behaviour with cindent
    if l:line =~ '#'
      return shiftwidth()
    endif
    if l:line =~ s:closeRulesSubsection
      return 0
    endif
    return cindent(v:lnum)
  endif

  if l:section == s:secondSection && VimLex_InEnclosedBlock(v:lnum)
    "fix weird behaviour with cindent
    if l:line =~ '}'
      return 0
    endif
    "fix weird behaviour with cindent
    if !VimLex_InEnclosedBlock(l:previousNum) && l:line =~ '[^}]'
      return shiftwidth()
    endif
    return cindent(v:lnum)
  endif

  if l:section == s:thirdSection
    return cindent(v:lnum)
  endif

  return 0
endfunction

function! VimLex_DetermineSection(lnum)
  " this function determines what section of lex the current line is on (there
  " are three general sections in lex)
  let l:sections = len(VimLex_FindAllToLine(a:lnum, s:sectionIndicator))

  if l:sections > s:thirdSection
    return s:thirdSection
  endif

  return l:sections
endfunction

function! VimLex_InRulesSubsection(lnum)
  " this function determines if the current line is within the rules subsection
  " of the first section of a lex file

  " two requirements to be in rules subsection:
  " 1. be in section 0
  if VimLex_DetermineSection(a:lnum) == s:firstSection
    " 2. check that you aren't between '%{' and '%}'
    call cursor(1,1)
    let l:open = search(s:openRulesSubsection, 'cW', a:lnum)

    if l:open == s:notFound
      return s:false
    endif

    let l:close = searchpair(s:openRulesSubsection,'',s:closeRulesSubsection, '', '', a:lnum)

    if l:close == s:notFound || l:close == a:lnum
      return s:true
    endif
  endif

  return s:false
endfunction

function! VimLex_FindAllToLine(lnum, regex)
  "this function finds a regex match up to a given line number
  call cursor(s:sof,s:sol)
  let l:matches = []

  while s:true
    let l:match = search(a:regex, "We") "see :h search
    if l:match != s:notFound && l:match < a:lnum
      call add(l:matches, l:match)
    else
      break
    endif
  endwhile

  return l:matches
endfunction

function! VimLex_InEnclosedBlock(lnum)
  call cursor(a:lnum, 1)
  let l:openBrace = search('{', 'bW')
  if l:openBrace != s:notFound
    let l:closeBrace = searchpair('{', '', '}', 'W')
    if (l:closeBrace >= a:lnum)
      return s:true
    endif
  endif
  return s:false
endfunction
